#!/usr/bin/env python
# -*- coding: utf-8 -*-

import ffmpeg
import glob
import argparse
import os

parser = argparse.ArgumentParser()
parser.add_argument("cote", help="Cote du dossier à traiter")
args = parser.parse_args()

dossier = args.cote
base_path = "/mnt/nas_archives/pieces"
preservation_path = base_path + '/preservation/audio/' + dossier
diffusion_path = base_path + '/diffusion/audio/' + dossier

liste_wav = glob.glob(preservation_path + '/*.wav')

def convert_file(file=None):
    file_name = os.path.splitext(os.path.basename(file))[0]
    (
        ffmpeg
        .input(file)
        .output(diffusion_path + '/' + file_name + '.mp3')
        .run()
    )

for f in liste_wav:
    convert_file(f)
